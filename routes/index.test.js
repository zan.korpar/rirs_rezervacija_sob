const reqest = require("supertest");
const app = require("../app");

describe("Test stražnika, ali vrne sobo!", () => {
    it("Številka sobe:", async () => {
        const res = await reqest(app)
        .get("/")
        .send();
        expect(res.statusCode).toEqual(200);
        expect(res.body).toHaveProperty("soba","102");
    })
})